// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import fastClick from 'fastclick'; // 移动端fastclick点击延迟

import VueAwesomeSwiper from 'vue-awesome-swiper'; // cnpm install vue-awesome-swiper
import 'swiper/dist/css/swiper.css';// vue-awesome-swiper的样式

import VueLazyLoad from 'vue-lazyload';  // cnpm install vue-lazyload图片懒加载


// 这里的 styles是路径的别名简写，可以在 build/webpack.base.conf.js中的 alias对象中进行设置
import 'styles/reset.css' // 引入reset.css样式,重置浏览器的默认样式
import 'styles/border.css'; // 引入border样式 1px边框的解决样式
import 'styles/iconfont.css'; // 引入下载好的阿里矢量图标的css样式

import axios from 'axios'

 import store from './store'; // 引入vuex并且在new vue中注入

Vue.config.productionTip = false;
fastClick.attach(document.body); // 使用移动端的fastclick
Vue.use(VueAwesomeSwiper); // 使用VueAwesomeSwiper

Vue.prototype.$axios = axios;


// 图片懒加载，加载前后加载错误的图片样式配置
Vue.use(VueLazyLoad,{
  error: require('./assets/images/error.jpg'),
  loading: require('./assets/images/loading.jpg')

});



/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
});
