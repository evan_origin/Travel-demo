import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'

// import Home from 'pages/home/home'

// 路由懒加载,异步按需加载路由
// 主页
const Home = (resolve) => {
  import('@/pages/home/home').then((module) => {
    resolve(module)
  });
};
// 城市选择页面
const City = (resolve) => {
  import('@/pages/city/City').then((module) => {
    resolve(module)
  })
};
const Detail = (resolve) => {
  import('@/pages/detail/Detail').then( (module) =>{
    resolve(module)
  })
};
Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },{
      path: '/city',
      name: 'City',
      component: City
    },{
      path: '/detail/:id',
      name: 'Detail',
      component: Detail
    }
  ],
  // 路由滚动行为 路由切换的时候滚动到最顶部
  scrollBehavior (to, from, savedPosition) {
    return {x: 0,y: 0}
  }
})
